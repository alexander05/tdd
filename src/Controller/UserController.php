<?php

namespace App\Controller;

use App\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;


class UserController extends FOSRestController
{
    const ACTIVE = 1;

    /**
     * @Rest\Get("/user/active")
     */
    public function getActiveAction()
    {
        $result = $this->getDoctrine()->getRepository(User::class)->findBy(['status' => self::ACTIVE]);

        $response = new Response();

        $serializer = $this->get('jms_serializer');
        $response->setContent($serializer->serialize($result,'json'));

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Rest\Get("/user")
     */
    public function getAction()
    {
        $result = $this->getDoctrine()->getRepository(User::class)->findAll();

        $response = new Response();

        $serializer = $this->get('jms_serializer');
        $response->setContent($serializer->serialize($result,'json'));

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Rest\Get("/user/{id}")
     */
    public function idAction($id)
    {
        $singleResult = $this->getDoctrine()->getRepository(User::class)->find($id);
        if ($singleResult === null) {
            return new Response(
                'User not found',
                Response::HTTP_NOT_FOUND
            );
        }

        $response = new Response();

        $serializer = $this->get('jms_serializer');
        $response->setContent($serializer->serialize($singleResult,'json'));

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
