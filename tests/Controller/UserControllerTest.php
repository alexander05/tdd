<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    const ACTIVE = 1;

    public function testListUserActive()
    {
        $client = static::createClient();

        $client->request('GET', '/user/active');

        $json = $client->getResponse()->getContent();

        $users = json_decode($json);

        $statuses = array_column($users, 'status');

        foreach ($statuses as $status)
        {
            $this->assertEquals(self::ACTIVE, $status);
        }
    }
}